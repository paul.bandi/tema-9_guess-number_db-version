<?php
// src/Controller/LuckyController.php
namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Session\Session;

class GuessNumber extends AbstractController
{
    
    /**
      * @Route("/guess/number")
      */
    public function guessNumber()
    {   
        $session = new Session();
        $session->start();
        $sessionNumber = $session->get('sessionNumber');
        if(!isset($sessionNumber)){
            $sessionNumber = random_int(0, 100);
            $session->set('sessionNumber',$sessionNumber);
        }
        
        return $this->render('guessNumber.html.twig', [
            'guessNumber' => $sessionNumber,
        ]);
    }


    /**
     * @Route("/reset/number")
     */
    public function resetNumber(){
        $session = new Session();
        $session->start();
        $sessionNumber = random_int(0, 100);
        $session->set('sessionNumber',$sessionNumber);
        return $this->render('guessNumber.html.twig', [
            'guessNumber' => $sessionNumber,
        ]);
    }
}